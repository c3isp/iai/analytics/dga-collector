package c3isp_dga_collector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;



public class Collector {

	static boolean SHOW_LOG = false;

	static String destination_filename_download = "dga_list.txt";
	static Statement stmt;
	static ResultSet rs;
	static DBconn state;



	public static void main(String[] args) {

		downloadList();

		state = new DBconn();
		stmt = state.getStatement();
		readDGAandWriteDB();

	}


	public static void readDGAandWriteDB()
	{
		int DGA_counter = 0;
		int i = 0;
		String DGA_url = "";
		String DGA_usedBy = "";
		String DGA_data = "";
		String DGA_info = "";
		try {
			System.out.println ("***************************");
			
			
			// Open the file
			FileInputStream fstream = new FileInputStream(destination_filename_download);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			
			/* TRUNCATE is faster than DELETE since
			 * it does not generate rollback information and does not
			 * fire any delete triggers
  			 */
			System.out.println ("> Deleting content of dga_known table");
			stmt.executeUpdate("TRUNCATE dga_known");
			
			
			String strLine, query = ""; 
			String[] queryBatch = new String[10000];
			Date date_DGA_usedBy;
			String[] lineSplit;

			DateFormat formatEng = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			

			System.out.println ("> Inserting DGA into the dga_known table in BATCH");
			//Read File Line By Line			
			while ((strLine = br.readLine()) != null)   {
				// Print the content on the console
				if (!strLine.startsWith("##"))
				{
					DGA_counter++;
					lineSplit = strLine.split(",");

					DGA_url = lineSplit[0];
					DGA_usedBy = lineSplit[1];
					DGA_data = lineSplit[2];
					DGA_info = lineSplit[3];
					/**
					 * To convert DGA_Data into a data format for MySQL
					 */
					date_DGA_usedBy = formatter.parse(DGA_data);
					
					if (SHOW_LOG)
					{
						System.out.println ("***************************");
						System.out.println ("DGA: "+ DGA_counter);
						System.out.println ("DGA_url: "+ DGA_url);
						System.out.println("Date_DGA_usedBy: "+ formatter.format(date_DGA_usedBy));	
						System.out.println ("DGA_data: "+ DGA_data);
						System.out.println ("DGA_info: "+ DGA_info);
						System.out.println ("***************************");
					}
					//					if (DGA_counter == 100)
					//					{
					//						System.exit(0);
					//					}

					query = "INSERT INTO dga_known " + "VALUES ("+DGA_counter+",\""+DGA_url+"\",\""+DGA_usedBy+"\",'"+formatEng.format(date_DGA_usedBy)+"',\""+DGA_info+"\")";
					
					/**
					 * Adding the query into the batch
					 */
					stmt.addBatch(query);

					if (SHOW_LOG)
					{
						System.out.println(query);
					}
					
					/**
					 * It prints every 10000 DGAs processed
					 */
					if (DGA_counter % 10000 == 0)

					{
						/**
						 * Inserting DGA into the DB using the batch
						 */
						stmt.executeBatch();
						System.out.println ("> Inserted #"+ DGA_counter+ " DGAs");
						stmt.clearBatch();
					}
				
					
//					stmt.executeUpdate(query);

				}
			}
			
			if (!query.isEmpty())
			{
				stmt.executeBatch();
				System.out.println ("> Inserted remaining DGAs");
			}
			
			
			System.out.println ("> Inserting DGA terminated, total #"+DGA_counter);
			System.out.println ("***************************");
			//Close the input stream
			br.close();
			
			/**
			 * Delete temp file
			 */
			
			new File(destination_filename_download).delete();
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * The following method download the list of DGA and save it into a file with the timestamp
	 */
	public static void downloadList()
	{


		String source = "https://osint.bambenekconsulting.com/feeds/dga-feed.txt";
		destination_filename_download = "/tmp/"+new SimpleDateFormat("'dga_list_'yyyy-MM-dd_hh-mm-ss'.txt'").format(new Date());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		
		
		System.out.println ("***************************");
		System.out.println("> Executing DGA collector from public source at "+dateFormat.format(date)); 
		System.out.println ("> Download DGA list started: "+destination_filename_download);
		try {
			URL website = new URL(source);
			ReadableByteChannel rbc = Channels.newChannel(website.openStream());
			FileOutputStream fos = new FileOutputStream(destination_filename_download);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println ("> Download DGA list terminated");
		System.out.println ("***************************");
	}

}
