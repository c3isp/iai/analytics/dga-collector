package c3isp_dga_collector;




import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DBconn {

	Statement stmt;
	Connection conn;


	/**
	 * Getting OS name
	 */
	private String OS = System.getProperty("os.name").toLowerCase();

	//Constructor
	public DBconn()
	{
		SetConnection();
	}

	//This method creates a connection to the DB
	private  void SetConnection()
	{
		try 
		{
			/**
			 * Driving DB connection depending on the depoyment type
			 */
			if ((OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 )) // if the API runs on IAI machine
			{
				System.out.println("> Connecting DB on IAI server");
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dga_list", "dgauser", "AXfr4+h+11-0022uy");
			}
			else if ((OS.indexOf("mac") >= 0)) // if the API is running on Mac OS
			{
				System.out.println("> Connecting DB on IAI server on mac os");
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dga_list", "root", "tezendae");;
			}
			
			stmt = conn.createStatement();
			System.out.println("> DB Connected");


		} 
		catch (Exception e) 
		{
			System.err.println("Got an exception!");
			e.printStackTrace();
		}
	}

	//This method returns a connection to the DB
	public Statement getStatement()
	{
		return stmt;
	}

	public Connection getConnection()
	{
		return conn;
	}

	public void closeConnectionDB()
	{
		try 
		{
			conn.close();
		} 
		catch (Exception e) 
		{
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
	}


}
